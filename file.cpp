#include <iostream>
#include <fstream>
 
using namespace std;
 
int main(int argc, char** argv)
{
  
    string line;
    //For writing text file
    //Creating ofstream & ifstream class object
    cout << argv[1] <<" "<< argv[2];
    string file1 = argv[1];
    string file2 = argv[2];
    ifstream ini_file {file1};
    ofstream out_file {file2};
 
    if(ini_file && out_file){
 
        while(getline(ini_file,line)){
            out_file << line << "\n";
        }
 
        cout << "\nCopy Finished \n";
 
    } else {
        //Something went wrong
        printf("Cannot read File");
    }
 
    //Closing file
    ini_file.close();
    out_file.close();
 
    return 0;
}
