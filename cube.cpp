#include <iostream>
using namespace std;

int cube(int a)
{
  int c = a * a * a;
  return c;
}

int main()
{
  int a, b;
  cout << "Please enter a number: ";
  cin >> a;

  b = cube(a);
  cout << b << "\n";
  return 0;
}
